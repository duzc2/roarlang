/**
 * @license
 * Copyright 2021 Google LLC
 * SPDX-License-Identifier: Apache-2.0
 */
Blockly.Themes.Halloween = Blockly.Theme.defineTheme('halloween', {
    'base': Blockly.Themes.Classic,
    'categoryStyles': {
        'list_category': {
            'colour': "#4a148c"
        },
        'logic_category': {
            'colour': "#8b4513",
        },
        'loop_category': {
            'colour': "#85E21F",
        },
        'text_category': {
            'colour': "#FE9B13",
        },
    },
    'blockStyles': {
        'list_blocks': {
            'colourPrimary': "#4a148c",
            'colourSecondary': "#AD7BE9",
            'colourTertiary': "#CDB6E9",
        },
        'logic_blocks': {
            'colourPrimary': "#8b4513",
            'colourSecondary': "#ff0000",
            'colourTertiary': "#C5EAFF"
        },
        'loop_blocks': {
            'colourPrimary': "#85E21F",
            'colourSecondary': "#ff0000",
            'colourTertiary': "#C5EAFF"
        },
        'text_blocks': {
            'colourPrimary': "#FE9B13",
            'colourSecondary': "#ff0000",
            'colourTertiary': "#C5EAFF"
        },
    },
    'componentStyles': {
        'workspaceBackgroundColour': '#ff7518',
        'toolboxBackgroundColour': '#F9C10E',
        'toolboxForegroundColour': '#fff',
        'flyoutBackgroundColour': '#252526',
        'flyoutForegroundColour': '#ccc',
        'flyoutOpacity': 1,
        'scrollbarColour': '#ff0000',
        'insertionMarkerColour': '#fff',
        'insertionMarkerOpacity': 0.3,
        'scrollbarOpacity': 0.4,
        'cursorColour': '#d0d0d0',
        'blackBackground': '#333'
    }
});
(function () {
    /**
     * Dark theme.
     */
    Blockly.Theme.defineTheme('dark', {
        base: Blockly.Themes.Classic,
        componentStyles: {
            workspaceBackgroundColour: '#1e1e1e',
            toolboxBackgroundColour: 'blackBackground',
            toolboxForegroundColour: '#fff',
            flyoutBackgroundColour: '#252526',
            flyoutForegroundColour: '#ccc',
            flyoutOpacity: 1,
            scrollbarColour: '#797979',
            insertionMarkerColour: '#fff',
            insertionMarkerOpacity: 0.3,
            scrollbarOpacity: 0.4,
            cursorColour: '#d0d0d0',
            blackBackground: '#333',
        },
    });
}());
(function () {
    Blockly.Theme.defineTheme('modern', {
        base: Blockly.Themes.Classic,
        blockStyles: {
            colour_blocks: {
                colourPrimary: '#a5745b',
                colourSecondary: '#dbc7bd',
                colourTertiary: '#845d49',
            },
            list_blocks: {
                colourPrimary: '#745ba5',
                colourSecondary: '#c7bddb',
                colourTertiary: '#5d4984',
            },
            logic_blocks: {
                colourPrimary: '#5b80a5',
                colourSecondary: '#bdccdb',
                colourTertiary: '#496684',
            },
            loop_blocks: {
                colourPrimary: '#5ba55b',
                colourSecondary: '#bddbbd',
                colourTertiary: '#498449',
            },
            math_blocks: {
                colourPrimary: '#5b67a5',
                colourSecondary: '#bdc2db',
                colourTertiary: '#495284',
            },
            procedure_blocks: {
                colourPrimary: '#995ba5',
                colourSecondary: '#d6bddb',
                colourTertiary: '#7a4984',
            },
            text_blocks: {
                colourPrimary: '#5ba58c',
                colourSecondary: '#bddbd1',
                colourTertiary: '#498470',
            },
            variable_blocks: {
                colourPrimary: '#a55b99',
                colourSecondary: '#dbbdd6',
                colourTertiary: '#84497a',
            },
            variable_dynamic_blocks: {
                colourPrimary: '#a55b99',
                colourSecondary: '#dbbdd6',
                colourTertiary: '#84497a',
            },
            hat_blocks: {
                colourPrimary: '#a55b99',
                colourSecondary: '#dbbdd6',
                colourTertiary: '#84497a',
                hat: 'cap',
            },
        },
        categoryStyles: {
            colour_category: {
                colour: '#a5745b',
            },
            list_category: {
                colour: '#745ba5',
            },
            logic_category: {
                colour: '#5b80a5',
            },
            loop_category: {
                colour: '#5ba55b',
            },
            math_category: {
                colour: '#5b67a5',
            },
            procedure_category: {
                colour: '#995ba5',
            },
            text_category: {
                colour: '#5ba58c',
            },
            variable_category: {
                colour: '#a55b99',
            },
            variable_dynamic_category: {
                colour: '#a55b99',
            },
        },
        componentStyles: {},
        fontStyle: {},
        startHats: null,
    });
}());
(function () {
    const defaultBlockStyles = {
        colour_blocks: {
            colourPrimary: '#a52714',
            colourSecondary: '#FB9B8C',
            colourTertiary: '#FBE1DD',
        },
        list_blocks: {
            colourPrimary: '#4a148c',
            colourSecondary: '#AD7BE9',
            colourTertiary: '#CDB6E9',
        },
        logic_blocks: {
            colourPrimary: '#01579b',
            colourSecondary: '#64C7FF',
            colourTertiary: '#C5EAFF',
        },
        loop_blocks: {
            colourPrimary: '#33691e',
            colourSecondary: '#9AFF78',
            colourTertiary: '#E1FFD7',
        },
        math_blocks: {
            colourPrimary: '#1a237e',
            colourSecondary: '#8A9EFF',
            colourTertiary: '#DCE2FF',
        },
        procedure_blocks: {
            colourPrimary: '#006064',
            colourSecondary: '#77E6EE',
            colourTertiary: '#CFECEE',
        },
        text_blocks: {
            colourPrimary: '#004d40',
            colourSecondary: '#5ae27c',
            colourTertiary: '#D2FFDD',
        },
        variable_blocks: {
            colourPrimary: '#880e4f',
            colourSecondary: '#FF73BE',
            colourTertiary: '#FFD4EB',
        },
        variable_dynamic_blocks: {
            colourPrimary: '#880e4f',
            colourSecondary: '#FF73BE',
            colourTertiary: '#FFD4EB',
        },
        hat_blocks: {
            colourPrimary: '#880e4f',
            colourSecondary: '#FF73BE',
            colourTertiary: '#FFD4EB',
            hat: 'cap',
        },
    };

    const categoryStyles = {
        colour_category: { colour: '#a52714' },
        list_category: { colour: '#4a148c' },
        logic_category: { colour: '#01579b' },
        loop_category: { colour: '#33691e' },
        math_category: { colour: '#1a237e' },
        procedure_category: { colour: '#006064' },
        text_category: { colour: '#004d40' },
        variable_category: { colour: '#880e4f' },
        variable_dynamic_category: { colour: '#880e4f' },
    };

    /**
     * High contrast theme.
     */
    Blockly.Theme.defineTheme('highcontrast', {
        blockStyles: defaultBlockStyles,
        categoryStyles: categoryStyles,
        componentStyles: {
            selectedGlowColour: '#000000',
            selectedGlowSize: 1,
            replacementGlowColour: '#000000',
        },
        fontStyle: {
            family: null, // Use default font-family.
            weight: null, // Use default font-weight.
            size: 16,
        },
        startHats: null,
    });
}());
(function () {
    /**
     * @fileoverview Tritanopia theme.
     */

    const defaultBlockStyles = {
        colour_blocks: {
            colourPrimary: '#05427f',
            colourSecondary: '#2974c0',
            colourTertiary: '#2d74bb',
        },
        list_blocks: {
            colourPrimary: '#b69ce8',
            colourSecondary: '#ccbaef',
            colourTertiary: '#9176c5',
        },
        logic_blocks: {
            colourPrimary: '#9fd2f1',
            colourSecondary: '#c0e0f4',
            colourTertiary: '#74bae5',
        },
        loop_blocks: {
            colourPrimary: '#aa1846',
            colourSecondary: '#d36185',
            colourTertiary: '#7c1636',
        },
        math_blocks: {
            colourPrimary: '#e6da39',
            colourSecondary: '#f3ec8e',
            colourTertiary: '#f2eeb7',
        },
        procedure_blocks: {
            colourPrimary: '#590721',
            colourSecondary: '#8c475d',
            colourTertiary: '#885464',
        },
        text_blocks: {
            colourPrimary: '#058863',
            colourSecondary: '#5ecfaf',
            colourTertiary: '#04684c',
        },
        variable_blocks: {
            colourPrimary: '#4b2d84',
            colourSecondary: '#816ea7',
            colourTertiary: '#83759e',
        },
        variable_dynamic_blocks: {
            colourPrimary: '#4b2d84',
            colourSecondary: '#816ea7',
            colourTertiary: '#83759e',
        },
    };

    const categoryStyles = {
        colour_category: {
            colour: '#05427f',
        },
        list_category: {
            colour: '#b69ce8',
        },
        logic_category: {
            colour: '#9fd2f1',
        },
        loop_category: {
            colour: '#aa1846',
        },
        math_category: {
            colour: '#e6da39',
        },
        procedure_category: {
            colour: '#590721',
        },
        text_category: {
            colour: '#058863',
        },
        variable_category: {
            colour: '#4b2d84',
        },
        variable_dynamic_category: {
            colour: '#4b2d84',
        },
    };

    /**
     * Tritanopia theme.
     * A colour palette for people that have tritanopia (the inability to perceive
     * blue light).
     */
    Blockly.Theme.defineTheme('tritanopia', {
        blockStyles: defaultBlockStyles,
        categoryStyles: categoryStyles,
        componentStyles: {},
        fontStyle: {},
        startHats: null,
    });
}());
(function () {
    /**
     * @fileoverview Deuteranopia theme.
     */

    const defaultBlockStyles = {
        colour_blocks: {
            colourPrimary: '#f2a72c',
            colourSecondary: '#f1c172',
            colourTertiary: '#da921c',
        },
        list_blocks: {
            colourPrimary: '#7d65ab',
            colourSecondary: '#a88be0',
            colourTertiary: '#66518e',
        },
        logic_blocks: {
            colourPrimary: '#9fd2f1',
            colourSecondary: '#c0e0f4',
            colourTertiary: '#74bae5',
        },
        loop_blocks: {
            colourPrimary: '#795a07',
            colourSecondary: '#ac8726',
            colourTertiary: '#c4a03f',
        },
        math_blocks: {
            colourPrimary: '#e6da39',
            colourSecondary: '#f3ec8e',
            colourTertiary: '#f2eeb7',
        },
        procedure_blocks: {
            colourPrimary: '#590721',
            colourSecondary: '#8c475d',
            colourTertiary: '#885464',
        },
        text_blocks: {
            colourPrimary: '#058863',
            colourSecondary: '#5ecfaf',
            colourTertiary: '#04684c',
        },
        variable_blocks: {
            colourPrimary: '#47025a',
            colourSecondary: '#820fa1',
            colourTertiary: '#8e579d',
        },
        variable_dynamic_blocks: {
            colourPrimary: '#47025a',
            colourSecondary: '#820fa1',
            colourTertiary: '#8e579d',
        },
    };

    const categoryStyles = {
        colour_category: {
            colour: '#f2a72c',
        },
        list_category: {
            colour: '#7d65ab',
        },
        logic_category: {
            colour: '#9fd2f1',
        },
        loop_category: {
            colour: '#795a07',
        },
        math_category: {
            colour: '#e6da39',
        },
        procedure_category: {
            colour: '#590721',
        },
        text_category: {
            colour: '#058863',
        },
        variable_category: {
            colour: '#47025a',
        },
        variable_dynamic_category: {
            colour: '#47025a',
        },
    };

    /**
     * Deuteranopia theme.
     * A colour palette for people that have deuteranopia (the inability to perceive
     * green light). This can also be used for people that have protanopia (the
     * inability to perceive red light).
     */
    Blockly.Theme.defineTheme('deuteranopia', {
        base: Blockly.Themes.Classic,
        blockStyles: defaultBlockStyles,
        categoryStyles: categoryStyles,
        componentStyles: {},
        fontStyle: {},
        startHats: null,
    });
}());
function setAllBlockStyles(styleArray) {
    for (let theme of Object.values(Blockly.registry.getAllItems(Blockly.registry.Type.THEME))) {
        for (let k in styleArray) {
            theme.blockStyles[k] = styleArray[k];
        }
    }
};

function setCategoryStyle(cateName, style) {
    for (let theme of Object.values(Blockly.registry.getAllItems(Blockly.registry.Type.THEME))) {
        theme.categoryStyles[cateName] = style;
    }
};
setAllBlockStyles({
    "fsm_blocks": {
        "colourPrimary": "#1e9c45"
    },
    "state_blocks": {
        "colourPrimary": "#a5745b"
    },
    "event_blocks": {// 兼容历史类型
        'colourPrimary': '330', 'hat': 'cap'
    },
    "native_blocks": {
        "colourPrimary": "#9c8251"
    },
    "message_blocks": {
        "colourPrimary": "#917840", 'hat': 'cap'
    },
    "fsm_variable_blocks": {
        "colourPrimary": "#272726"
    },
    "state_variable_blocks": {
        "colourPrimary": "#272726"
    },
});
setCategoryStyle("fsm_category", {
    "colour": "#1e9c45"
});
setCategoryStyle("state_category", {
    "colour": "#a5745b"
});
setCategoryStyle("search_category", {
    "colour": "#00000000"
});

new Blockly.Theme('black');

new Blockly.Theme('千里江山',
    // block styles
    {
        "fsm_blocks": {
            "colourPrimary": "#9c8251"
        },
        "text_blocks": {
            "colourPrimary": "#545b31"
        },
        "logic_blocks": {
            "colourPrimary": "#677b60"
        },
        "colour_blocks": {
            "colourPrimary": "#6d7e52"
        },
        "loop_blocks": {
            "colourPrimary": "#2e8acd"
        },
        "math_blocks": {
            "colourPrimary": "#152b50"
        },
        "variable_blocks": {
            "colourPrimary": "#876831",
        },
        "variable_dynamic_blocks": {
            "colourPrimary": "#836a28"
        },
        "hat_blocks": {
            "colourPrimary": "#917840", 'hat': 'cap'
        },
        "event_blocks": {// 兼容历史类型
            'colourPrimary': '#917840', 'hat': 'cap'
        },
        "procedure_blocks": {
            "colourPrimary": "#688f62"
        },
        "state_blocks": {
            "colourPrimary": "#272726"
        },
        "variable_blocks": {
            "colourPrimary": "#39401f"
        },
        "list_blocks": {
            "colourPrimary": "#9c8251"
        },
        "native_blocks": {
            "colourPrimary": "#9c8251"
        },
        "message_blocks": {
            "colourPrimary": "#917840", 'hat': 'cap'
        },
        "fsm_variable_blocks": {
            "colourPrimary": "#272726"
        },
        "state_variable_blocks": {
            "colourPrimary": "#272726"
        },
    },
    // category styles
    {
        "list_category": {
            "colour": "#9c8251"
        },
        "variable_category": {
            "colour": "#39401f"
        },
        "state_category": {
            "colour": "#272726"
        },
        "procedure_category": {
            "colour": "#688f62"
        },
        "colour_category": {
            "colour": "#6d7e52"
        },
        "variable_dynamic_category": {
            "colour": "#836a28"
        },
        "math_category": {
            "colour": "#152b50"
        },
        "loop_category": {
            "colour": "#2e8acd"
        },
        "fsm_category": {
            "colour": "#9c8251"
        },
        "logic_category": {
            "colour": "#677b60"
        },
        "text_category": {
            "colour": "#545b31"
        },
        "native_category": {
            "colour": "#545b31"
        },
    },
    // component styles
    {
        "workspaceBackgroundColour": "#f4eddd",
        "toolboxBackgroundColour": "#c5bca1",
        "toolboxForegroundColour": "#000",
        "flyoutBackgroundColour": "#c5bca1",
    }
);
const MinimalisticGradient_blockColors = [
    "#A67CBF", // Lighter shade closer to background
    "#9C6EB8",
    "#9263B1",
    "#8858AA",
    "#7E4D9F",
    "#744298",
    "#6A3790",
    "#602C89",
    "#562182",
    "#4C167B",
    "#420B74",
    "#38006D",
    "#40408A", // Transition towards main UI color
    "#50589A",
    "#6070AB"
];

let i = 0;
new Blockly.Theme('MinimalisticGradient', {
    "hat_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i], 'hat': 'cap'
    },
    "event_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++], 'hat': 'cap'
    },
    "message_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "fsm_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "text_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "logic_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "loop_blocks": {
        'colourPrimary': MinimalisticGradient_blockColors[i++]
    },
    "math_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "colour_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "procedure_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "variable_dynamic_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "fsm_variable_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "state_variable_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "variable_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "list_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
    "native_blocks": {
        "colourPrimary": MinimalisticGradient_blockColors[i++]
    },
}, {}, {
    "workspaceBackgroundColour": "rgba(227, 214, 255, 0.55)",
    "toolboxBackgroundColour": "rgb(83, 83, 117)",
    "toolboxForegroundColour": "rgb(255, 255, 255)",
    "flyoutBackgroundColour": "rgb(152, 151, 181)",
});
OpenBlock.Utils.addCssFile('./core/ub/themes/minimalistic.css');
