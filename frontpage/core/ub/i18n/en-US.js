OpenBlock.I18N.START_SRC_NAME = 'Start';
OpenBlock.I18N.NEW_SRC_NAME = 'New Module';
OpenBlock.I18N['Click to set project name'] = 'Click to set project name';
OpenBlock.I18N.START_FSM_NAME = 'Main';
OpenBlock.I18N.NEW_FSM_TYPE_NAME = 'New FSM Type';
OpenBlock.I18N.NEW_STATE_NAME = 'State';
OpenBlock.I18N.NEW_ACTION_GROUP_NAME = 'Action Group';
OpenBlock.I18N.NEW_STRUCT_NAME = 'Struct';
OpenBlock.I18N.NEW_FUNCTION_NAME = 'Function';
OpenBlock.I18N.Number = 'Number';
OpenBlock.I18N.Integer = 'Integer';
OpenBlock.I18N.Boolean = 'Boolean';
OpenBlock.I18N.String = 'String';
OpenBlock.I18N.FSM = 'FSM';
OpenBlock.I18N.COLOR = 'Color';

Blockly.Msg["value"] = "value";
Blockly.Msg["VALUE"] = "Value";

Blockly.Msg["Colour"] = "Color";
Blockly.Msg["Number"] = "Number";
Blockly.Msg["String"] = "String";
Blockly.Msg["Boolean"] = "Boolean";
Blockly.Msg["Integer"] = "Integer";
Blockly.Msg["MESSAGE"] = "Message";
Blockly.Msg["FSM_CTRL"] = "State machine control";
Blockly.Msg["EVENT"] = "Event";
Blockly.Msg["SEARCH"] = "Search";
Blockly.Msg["CLEAR"] = "Clear";
Blockly.Msg["DEBUG"] = "Debug";
Blockly.Msg["TEXT"] = "Text";
Blockly.Msg["BOOLEAN"] = "Boolean";
Blockly.Msg["LOOPS"] = "Loops";
Blockly.Msg["MATH"] = "Math";
Blockly.Msg["LISTS"] = "List";
Blockly.Msg["STRUCTS"] = "Structs";
Blockly.Msg["METHOD"] = "Method";
Blockly.Msg["VAR_FSM"] = "FSM Variables";
Blockly.Msg["VAR_STATE"] = "State Variables";
Blockly.Msg["VAR_LOCAL"] = "Local Variables";
Blockly.Msg["local_variable_get"] = "Local Variable %1";
Blockly.Msg["local_variable_set"] = "Set Local Variable to %1";
Blockly.Msg["COLLECT"] = "Collect";
Blockly.Msg["ASSETS"] = "Assets";
Blockly.Msg["FSM"] = "FSM";
Blockly.Msg["Start"] = "Start";
Blockly.Msg["Restore"] = "Restore";
Blockly.Msg["Grid"] = "Grid";
Blockly.Msg["LOGIC_VAR_ASSIGNED"] = "The variable is assigned %1";
Blockly.Msg["LOGIC_VAR_ASSIGNED_TOOLTIP"] = "Whether the assignment";
Blockly.Msg["NEW_STRUCTS"] = "create struct";
Blockly.Msg["DEF_STRUCTS"] = "Defining data structures";
Blockly.Msg["OP_STRUCTS"] = "Manipulative data structure";
Blockly.Msg["OP_STRUCT_LIST"] = "Operation list";
Blockly.Msg["OP_STRUCT_SMAP"] = "Manipulating string mapping";
Blockly.Msg["OP_STRUCT_IMAP"] = "Manipulating integer mapping";
Blockly.Msg["TEXT_LOG"] = "Log %2 level %1";
OpenBlock.I18N.PRIMARY_TYPES = [
    [OpenBlock.I18N.Number, 'Number'],
    [OpenBlock.I18N.Integer, 'Integer'],
    [OpenBlock.I18N.Boolean, 'Boolean'],
    [OpenBlock.I18N.String, 'String'],
    [OpenBlock.I18N.FSM, 'FSM'],
    [OpenBlock.I18N.COLOR, 'Colour']
];
Blockly.Msg["COLOR"] = OpenBlock.I18N.COLOR;
Blockly.Msg["colour"] = OpenBlock.I18N.COLOR;
Blockly.Msg["text"] = OpenBlock.I18N.String;
Blockly.Msg["math_integer"] = "Integer %1";
Blockly.Msg["destroy_fsm"] = "Destroy FSM";
Blockly.Msg["variables_self"] = "Current FSM";
Blockly.Msg["logic_is_not_valid"] = "%1 is not valid";
Blockly.Msg["logic_is_valid"] = "%1 is valid";
Blockly.Msg["controls_yield"] = "yield";
Blockly.Msg["change_state"] = "change state %1";
Blockly.Msg["push_state"] = "push state %1";
Blockly.Msg["pop_state"] = "pop state";
Blockly.Msg["on_message"] = "On message received %1";
Blockly.Msg["received_message_arg"] = "received %2 %1";
Blockly.Msg["on_message_with_arg"] = "On message received %1 with %2";
Blockly.Msg["on_message_struct"] = "On message received %1 with struct %2";
Blockly.Msg["on_message_primary"] = "On message received %1 with primary %2";
Blockly.Msg["fsm_variables_get"] = "get FSM variable %2 %1";
Blockly.Msg["fsm_variables_set"] = "set FSM variable %3 %1 为 %2";
Blockly.Msg["state_variables_get"] = "State variable %2 %1";
Blockly.Msg["state_variables_set"] = "Set the state variable %3 %1 to %2";
Blockly.Msg["fsm_create"] = "Create %1 type of state machine";
Blockly.Msg["fsm_create_unnamed"] = "Unnamed";
Blockly.Msg["find_fsm_by_type"] = "Search for a state machine of type %1";
Blockly.Msg["struct_count_in_dataset"] = "The number of %1 in the data set";

Blockly.Msg["network.Network_join"] = "Join the network";
Blockly.Msg["network.Network_is_joined"] = "Joined the network";
Blockly.Msg["network.Network_leave"] = "Disconnect the network";
Blockly.Msg["Network_send_message"] = "Sending a remote message";
Blockly.Msg["Network_target"] = "The remote target";
Blockly.Msg["Network_on_received"] = "When a remote message is received";
Blockly.Msg["network_peer_join"] = "When a remote target is available";
Blockly.Msg["network_peer_leave"] = "When the remote target is offline";
Blockly.Msg["Network_is_network_message"] = "Whether the current message is from a remote source";
Blockly.Msg["Network_enable"] = "Receiving Network Messages";
Blockly.Msg["network.Network_set_enable"] = "Sets the current state machine to receive network messages";
Blockly.Msg["network.Network_is_enabled"] = "Whether to accept network messages";
Blockly.Msg["math_binary_bit_operator"] = "Bitwise operator %1 %2 %3";
Blockly.Msg["math_bit_operator_NOT"] = "Bitwise NOT %1";


Blockly.Msg["target"] = "target";
Blockly.Msg["data"] = "data";
Blockly.Msg["any"] = "any";
Blockly.Msg["enabled"] = "enabled";
Blockly.Msg["offset"] = "offset";

Blockly.Msg['Simulator'] = 'Simulator';
Blockly.Msg['logic_operation_and'] = "AND";

Blockly.Msg['math_text_to_integer'] = "text to integer %1";
Blockly.Msg['math_text_to_number'] = "text to number %1";


Blockly.Msg["replyTitle"] = "reply title";
Blockly.Msg["feature"] = "feature";

//blockly原版

Blockly.Msg["TEXT_GET_SUBSTRING_END_FROM_END"] = "To the penultimate # character";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDDOWN"] = "Take down the whole";
Blockly.Msg["MATH_ROUND_OPERATOR_ROUNDUP"] = "Take up the whole";
Blockly.Msg["MATH_RANDOM_INT_TITLE"] = "Random integer [%1,%2)";
