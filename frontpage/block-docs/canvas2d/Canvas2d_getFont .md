# 获取字体

说明：
- 获取当前字符串使用的字体

示例：

![alt text](img/getFont.png)

打印输出了当前使用的字体信息。