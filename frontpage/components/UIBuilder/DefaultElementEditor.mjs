
import { defineComponent, defineAsyncComponent, resolveComponent, nextTick } from 'vue';
let DefaultElementEditor = defineAsyncComponent(async () => {
    return {
        template: (await axios('./components/UIBuilder/DefaultElementEditor.html')).data,
        props: {
            element: { type: Object }
        }
    }
});
export default DefaultElementEditor;