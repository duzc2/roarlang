#include "binary_buffer.h"
#include <stdio.h>

int main() {
    // 使用内部缓冲区创建一个大小为 1024 的缓冲区
    BinaryBuffer* buffer = createBuffer(1024, NULL);
    writeInt32(buffer, 42);
    printf("Data length: %zu\n", getDataLength(buffer)); // 输出：4
    printf("Buffer length: %zu\n", getBufferLength(buffer)); // 输出：1024

    // 写入不同类型的数据
    writeInt32(buffer, -123456789);
    writeUint32(buffer, 123456789);
    writeUint8(buffer, 255);

    // 读取数据
    buffer->readPos = 0;
    printf("Int32: %d\n", readInt32(buffer));
    printf("Uint32: %u\n", readUint32(buffer));
    printf("Uint8: %u\n", readUint8(buffer));

    // 使用外部缓冲区创建缓冲区
    uint8_t externalBuffer[12] = {0};
    BinaryBuffer* extBuffer = createBuffer(sizeof(externalBuffer), externalBuffer);

    // 写入外部缓冲区
    writeInt32(extBuffer, -987654321);
    writeUint32(extBuffer, 987654321);
    writeUint8(extBuffer, 127);

    // 读取外部缓冲区的数据
    extBuffer->readPos = 0;
    printf("Ext Int32: %d\n", readInt32(extBuffer));
    printf("Ext Uint32: %u\n", readUint32(extBuffer));
    printf("Ext Uint8: %u\n", readUint8(extBuffer));

    // 释放内部缓冲区
    freeBuffer(buffer);

    // 外部缓冲区由用户管理，因此不需要释放
    free(extBuffer);

    return 0;
}
