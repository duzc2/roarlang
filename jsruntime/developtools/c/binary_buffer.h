#ifndef BINARY_BUFFER_H
#define BINARY_BUFFER_H

#include <stdint.h>
#include <stdlib.h>

// 定义 BinaryBuffer 结构体
typedef struct {
    uint8_t* buffer;      // 内部缓冲区指针
    size_t capacity;      // 缓冲区总大小
    size_t writePos;      // 当前写入位置
    size_t readPos;       // 当前读取位置
    int externalBuffer;   // 标志是否使用外部缓冲区
} BinaryBuffer;

// 创建缓冲区，支持传入已有的 uint8_t 数组或指定大小
BinaryBuffer* createBuffer(size_t size, uint8_t* array);

// 获取当前数据长度
size_t getDataLength(BinaryBuffer* b);

// 获取缓冲区总长度
size_t getBufferLength(BinaryBuffer* b);

// 确保缓冲区有足够空间（仅在使用内部缓冲区时有效）
void ensureSize(BinaryBuffer* b, size_t size);

// 写入函数
void writeInt32(BinaryBuffer* b, int32_t value);
void writeUint32(BinaryBuffer* b, uint32_t value);
void writeUint8(BinaryBuffer* b, uint8_t value);
void writeFloat32(BinaryBuffer* b, float value);

// 读取函数
int32_t readInt32(BinaryBuffer* b);
uint32_t readUint32(BinaryBuffer* b);
uint8_t readUint8(BinaryBuffer* b);
float readFloat32(BinaryBuffer* b);

// 按索引读取函数
int32_t readInt32At(BinaryBuffer* b, size_t index);
uint32_t readUint32At(BinaryBuffer* b, size_t index);
uint8_t readUint8At(BinaryBuffer* b, size_t index);
float readFloat32At(BinaryBuffer* b, size_t index);

// 按索引写入函数
void writeInt32At(BinaryBuffer* b, size_t index, int32_t value);
void writeUint32At(BinaryBuffer* b, size_t index, uint32_t value);
void writeUint8At(BinaryBuffer* b, size_t index, uint8_t value);
void writeFloat32At(BinaryBuffer* b, size_t index, float value);

// 清理缓冲区
void freeBuffer(BinaryBuffer* b);

#endif // BINARY_BUFFER_H
