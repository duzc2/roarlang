
import { h, defineComponent, reactive } from 'vue';
function r(node, override) {
    if (node) {
        let attr = override ? override(node) : node.attr;
        if (!attr) {
            attr = node.attr || {};
        }
        attr = Object.assign(attr, {
            id: node.id,
            ref: node.id
        });
        if (node.children && node.children.length > 0) {
            return h(node.elementType, attr, node.children.map(c => r(reactive(c))));
        } else {
            return h(node.elementType, attr, []);
        }
    } else {
        return null;
    }
}
let UIBuilderRT = defineComponent({
    props: { page: { type: Object }, override: { type: Function } },
    render() {
        return r(reactive(this.page), this.override);
    },
    data() {
        return {
        };
    }
});
export default UIBuilderRT;