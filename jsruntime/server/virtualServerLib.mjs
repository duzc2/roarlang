import * as obvm from '../../jsruntime/runtime/vm.mjs'

export default class VirtualServer {
    static install(script) {
        let server = new VirtualServer();
        script.InstallLib("CSNetwork-Server", "CSNetwork-Server", [
            script.NativeUtil.closureVoid(server.listenClientMessage.bind(server), [], true),
            script.NativeUtil.closureReturnValue(server.isNetworkMessage.bind(server), 'LongRegister', [], true),
        ]);
    }
    constructor() {
    }
    listenClientMessage(state, roarfunction, localvar) {
        let fsm = state.fsm;
        let vm = fsm.VM;
        vm.__virtualServerPortal = fsm;
    }
    isNetworkMessage(state, roarfunction, localvar) {
        return !!(state?.currentMessage?.__virtual_pfsm);
    }
}