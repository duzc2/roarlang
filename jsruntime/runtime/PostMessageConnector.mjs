import { DebuggerBroker } from './debugger.mjs';
class PostMessageConnector extends DebuggerBroker {
    constructor(brokerOptions) {
        super(brokerOptions);
        window.addEventListener('message', (evt) => {
            if (evt.data && evt.data.cmd === 'debug') {
                this.onMessage(evt.data);
            }
        });

    }
    onEvent(name, args, level, block, stack) {
        super.onEvent(name, block, args, level, stack);
        try {
            window.parent.postMessage({
                cmd: 'debug',
                name, args,
                level, stack, block
            }, '*');
        } catch (e) {
            console.error(e);
            debugger
            throw e;
        }
    }
}
export { PostMessageConnector }